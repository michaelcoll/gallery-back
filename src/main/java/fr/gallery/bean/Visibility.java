package fr.gallery.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Visibility {

    @EqualsAndHashCode.Include
    private long id;
    private String name;
    private User owner;

    public Visibility(long id) {
        this.id = id;
    }

}
