package fr.gallery.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

    @EqualsAndHashCode.Include
    private long id;
    private boolean active;
    private String username;
    private String password;
    private String email;

    public User(long id) {
        this.id = id;
    }

}
