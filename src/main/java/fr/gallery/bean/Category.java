package fr.gallery.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Category {

    @EqualsAndHashCode.Include
    private long id;
    private String name;
    private String description;
    private User owner;
    private Category parent;
    private Visibility visibility;
    private Media representativeMedia;

}
