package fr.gallery.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Media {

    @EqualsAndHashCode.Include
    private long id;
    private String hash;
    private String diskLocation;
    private String name;
    private String description;
    private String mimeType;
    private MediaType type;
    private LocalDateTime insertDate;
    private LocalDateTime dateOriginal;
    private Double longitude;
    private Double latitude;
    private User owner;
    private Visibility visibility;

}
