package fr.gallery.bean;

public enum MediaType {

    UNKNOWN,
    PHOTO,
    VIDEO

}
