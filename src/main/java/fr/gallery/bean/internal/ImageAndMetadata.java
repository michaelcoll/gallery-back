package fr.gallery.bean.internal;

import com.drew.lang.GeoLocation;
import fr.gallery.bean.MediaType;
import fr.gallery.metadata.MetadataWrapper;
import lombok.Data;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Data
public class ImageAndMetadata {

    private final File file;
    private final MetadataWrapper metadata;
    private String hash;
    private LocalDateTime dateTime;
    private GeoLocation geoLocation;
    private String mimeType;
    private MediaType mediaType;

    public boolean hasMetadata() {
        return mimeType != null;
    }

    public Optional<GeoLocation> getGeoLocation() {
        return ofNullable(geoLocation);
    }
}
