package fr.gallery.controller;

import fr.gallery.bean.Media;
import fr.gallery.service.MediaService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/media")
@RequiredArgsConstructor
public class MediaController {

    private final MediaService mediaService;

    @GetMapping(value = "/{hash}")
    public ResponseEntity<Flux<DataBuffer>> byHash(@PathVariable String hash) {

        return mediaService.byHash(hash)
            .map(media -> ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(media.getMimeType()))
                .body(DataBufferUtils.read(
                    new FileSystemResource(media.getDiskLocation()),
                    new DefaultDataBufferFactory(), StreamUtils.BUFFER_SIZE)))
            .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public Flux<Media> list() {
        return mediaService.all();
    }

    @GetMapping("/count")
    public Mono<Integer> count() {
        return mediaService.count();
    }

}
