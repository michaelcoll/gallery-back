package fr.gallery.utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.imaging.quicktime.QuickTimeMetadataReader;
import com.drew.lang.annotations.NotNull;
import com.drew.metadata.Metadata;
import com.drew.metadata.file.FileSystemMetadataReader;
import fr.gallery.metadata.MetadataWrapper;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.math.BigInteger;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

@Slf4j
public final class IndexUtils {

    private IndexUtils() {
    }

    public static Stream<File> listFiles(String path) {
        return listFiles(new File(path));
    }

    private static Stream<File> listFiles(File folder) {
        return ofNullable(folder.listFiles())
            .stream()
            .flatMap(Arrays::stream)
            .flatMap(file -> {
                if (file.isDirectory()) {
                    return listFiles(file);
                } else {
                    return Stream.of(file);
                }
            });
    }

    public static Optional<MetadataWrapper> getMetadata(File file) {
        try {
            return of(new MetadataWrapper(readMetadata(file)));
        } catch (ImageProcessingException | IOException e) {
            log.warn(file.getAbsolutePath() + " : " + e.getMessage());
            return Optional.empty();
        }
    }

    /**
     * Reads {@link Metadata} from a {@link File} object.
     *
     * @param file a file from which the image data may be read.
     * @return a populated {@link Metadata} object containing directories of tags with values and any processing errors.
     * @throws ImageProcessingException for general processing errors.
     */
    @NotNull
    private static Metadata readMetadata(@NotNull final File file) throws ImageProcessingException, IOException {
        Metadata metadata;
        try (InputStream inputStream = new FileInputStream(file)) {
            if (file.getPath().endsWith(".MOV") || file.getPath().endsWith(".mov")) {
                metadata = QuickTimeMetadataReader.readMetadata(inputStream);
            } else {
                metadata = ImageMetadataReader.readMetadata(inputStream, file.length());
            }
        } catch (ImageProcessingException e) {
            throw e;
        } catch (Exception e) {
            log.error(file.getAbsolutePath() + " : " + e.getMessage(), e);
            throw new ImageProcessingException(e);
        }
        new FileSystemMetadataReader().read(file, metadata);
        return metadata;
    }

    public static String hashFile(File f) throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        try (BufferedInputStream in = new BufferedInputStream((new FileInputStream(f)));
             DigestOutputStream out = new DigestOutputStream(OutputStream.nullOutputStream(), md)) {
            in.transferTo(out);
        }

        String fx = "%0" + (md.getDigestLength() * 2) + "x";
        return String.format(fx, new BigInteger(1, md.digest()));
    }
}
