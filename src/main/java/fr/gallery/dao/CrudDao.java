package fr.gallery.dao;

import java.util.List;

/**
 * Interface générique pour les dao de type CRUD.
 *
 * @param <PK>   la primary key de la table.
 * @param <TYPE> le type de la table.
 */
public interface CrudDao<PK, TYPE> {

    /**
     * Retourne tout les éléments de la table.
     *
     * @return tout les éléments de la table.
     */
    List<TYPE> all();

    /**
     * Retourne le nombre d'enregistrement de la table.
     *
     * @return le nombre d'enregistrement de la table.
     */
    int count();

    /**
     * Retourne l'enregistrement lié à la primary key passé en param.
     *
     * @param id un id
     * @return l'enregistrement lié à la primary key passé en param.
     */
    TYPE byId(PK id);

    /**
     * Créé un nouvel enregistrement en base.
     *
     * @param bean l'objet à insérer en base.
     * @return le nombre d'enregistrements insérés.
     */
    void create(TYPE bean);

    /**
     * Met à jour un objet en base.
     *
     * @param bean l'objet à sauvegarder.
     */
    void update(TYPE bean);

    /**
     * Supprime un objet en base.
     *
     * @param id l'id de l'objet à supprimer en base.
     */
    void delete(PK id);

    /**
     * Retourne {@code true} si l'enregistrement existe en base.
     *
     * @param id un id d'enregistrement.
     * @return {@code true} si l'enregistrement existe en base.
     */
    default boolean exists(PK id) {
        return byId(id) != null;
    }
}
