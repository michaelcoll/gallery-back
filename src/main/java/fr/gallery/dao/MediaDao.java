package fr.gallery.dao;

import fr.gallery.bean.Media;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MediaDao extends CrudDao<Long, Media> {

    List<Media> byHash(String hash);

    int countByHashAndFileName(Media media);
}
