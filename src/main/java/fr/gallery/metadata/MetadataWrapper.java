package fr.gallery.metadata;

import com.drew.lang.annotations.NotNull;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
public class MetadataWrapper {
    private final Metadata metadata;

    public <T extends Directory> Optional<T> getFirstDirectoryOfType(@NotNull Class<T> type) {
        return ofNullable(metadata.getFirstDirectoryOfType(type));
    }
}
