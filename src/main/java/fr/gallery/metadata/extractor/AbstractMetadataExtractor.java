package fr.gallery.metadata.extractor;

import com.drew.metadata.Directory;
import fr.gallery.bean.internal.ImageAndMetadata;

public abstract class AbstractMetadataExtractor<T extends Directory> {

    public void extract(ImageAndMetadata imageAndMetadata) {
        imageAndMetadata.getMetadata()
            .getFirstDirectoryOfType(getDirectoryClass())
            .ifPresent(directory -> extractMetadata(imageAndMetadata, directory));
    }

    abstract Class<T> getDirectoryClass();

    abstract void extractMetadata(ImageAndMetadata imageAndMetadata, T directory);
}
