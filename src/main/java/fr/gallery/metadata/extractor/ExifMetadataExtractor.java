package fr.gallery.metadata.extractor;

import com.drew.metadata.exif.ExifSubIFDDirectory;
import fr.gallery.bean.internal.ImageAndMetadata;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

import static java.util.Optional.ofNullable;

@Component
public class ExifMetadataExtractor extends AbstractMetadataExtractor<ExifSubIFDDirectory> {

    @Override
    Class<ExifSubIFDDirectory> getDirectoryClass() {
        return ExifSubIFDDirectory.class;
    }

    @Override
    void extractMetadata(ImageAndMetadata imageAndMetadata, ExifSubIFDDirectory directory) {
        ofNullable(directory.getDateOriginal(TimeZone.getTimeZone("GMT")))
            .map(dateOriginal -> LocalDateTime.ofInstant(dateOriginal.toInstant(), ZoneId.of("GMT")))
            .ifPresent(imageAndMetadata::setDateTime);
    }
}
