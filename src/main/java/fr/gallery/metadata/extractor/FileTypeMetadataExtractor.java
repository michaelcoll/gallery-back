package fr.gallery.metadata.extractor;

import com.drew.metadata.file.FileTypeDirectory;
import fr.gallery.bean.MediaType;
import fr.gallery.bean.internal.ImageAndMetadata;
import org.springframework.stereotype.Component;

import static com.drew.metadata.file.FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE;
import static com.drew.metadata.file.FileTypeDirectory.TAG_DETECTED_FILE_TYPE_NAME;

@Component
public class FileTypeMetadataExtractor extends AbstractMetadataExtractor<FileTypeDirectory> {

    @Override
    Class<FileTypeDirectory> getDirectoryClass() {
        return FileTypeDirectory.class;
    }

    @Override
    void extractMetadata(ImageAndMetadata imageAndMetadata, FileTypeDirectory directory) {
        imageAndMetadata.setMimeType(directory.getString(TAG_DETECTED_FILE_MIME_TYPE));
        imageAndMetadata.setMediaType(getType(directory.getString(TAG_DETECTED_FILE_TYPE_NAME)));
    }

    private static MediaType getType(String type) {
        switch (type) {
            case "JPEG":
                return MediaType.PHOTO;
            default:
                return MediaType.UNKNOWN;
        }

    }
}
