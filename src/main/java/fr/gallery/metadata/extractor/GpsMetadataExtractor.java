package fr.gallery.metadata.extractor;

import com.drew.metadata.exif.GpsDirectory;
import fr.gallery.bean.internal.ImageAndMetadata;
import org.springframework.stereotype.Component;

@Component
public class GpsMetadataExtractor extends AbstractMetadataExtractor<GpsDirectory> {

    @Override
    Class<GpsDirectory> getDirectoryClass() {
        return GpsDirectory.class;
    }

    @Override
    void extractMetadata(ImageAndMetadata imageAndMetadata, GpsDirectory directory) {
        imageAndMetadata.setGeoLocation(directory.getGeoLocation());
    }
}
