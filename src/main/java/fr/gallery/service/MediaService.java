package fr.gallery.service;

import fr.gallery.bean.Media;
import fr.gallery.dao.MediaDao;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MediaService extends AbstractCrudService<Long, Media, MediaDao> {

    protected MediaService(MediaDao dao) {
        super(dao);
    }

    public Optional<Media> byHash(String hash) {
        return dao.byHash(hash).stream().findFirst();
    }

    public boolean doesNotExistsByHashAndFileName(Media media) {
        return dao.countByHashAndFileName(media) == 0;
    }
}
