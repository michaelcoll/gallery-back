package fr.gallery.service;

import com.drew.metadata.Directory;
import com.google.common.base.Stopwatch;
import fr.gallery.bean.Media;
import fr.gallery.bean.internal.ImageAndMetadata;
import fr.gallery.config.GalleryParams;
import fr.gallery.metadata.extractor.AbstractMetadataExtractor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static fr.gallery.utils.IndexUtils.getMetadata;
import static fr.gallery.utils.IndexUtils.hashFile;
import static fr.gallery.utils.IndexUtils.listFiles;

@Slf4j
@Service
@RequiredArgsConstructor
public class IndexerService {

    private final MediaService service;
    private final GalleryParams params;
    private final List<AbstractMetadataExtractor<? extends Directory>> extractors;

    @Scheduled(initialDelay = 5000, fixedDelay = Long.MAX_VALUE)
    protected void init() {
        reIndex();
    }

    @Async
    public void reIndex() {
        Stopwatch stopwatch = Stopwatch.createStarted();

        log.info("Starting re-indexing...");

        log.info("Cleaning removed media ...");
        service.all().toStream()
            .filter(media -> !new File(media.getDiskLocation()).exists())
            .forEach(media -> service.delete(media.getId()));

        log.info("Scanning for new media ...");
        listFiles(params.getBaseDir())
            .parallel()
            .flatMap(file -> loadMetadata(file).stream())
            .filter(ImageAndMetadata::hasMetadata)
            .map(IndexerService::toMedia)
            .filter(service::doesNotExistsByHashAndFileName)
            .forEach(service::create);

        log.info("Indexed {} images in {}ms", service.count().block(), stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    private Optional<ImageAndMetadata> loadMetadata(File file) {

        return getMetadata(file)
            .map(metadata -> new ImageAndMetadata(file, metadata))
            .map(imageAndMetadata -> {
                extractors.forEach(extractor -> extractor.extract(imageAndMetadata));
                return imageAndMetadata;
            })
            .map(imageAndMetadata -> {
                try {
                    imageAndMetadata.setHash(hashFile(file));
                } catch (IOException | NoSuchAlgorithmException e) {
                    throw new RuntimeException(e);
                }
                return imageAndMetadata;
            });
    }

    private static Media toMedia(ImageAndMetadata imageAndMetadata) {
        Media media = new Media();
        media.setHash(imageAndMetadata.getHash());
        media.setName(imageAndMetadata.getFile().getName());
        media.setDiskLocation(imageAndMetadata.getFile().getAbsolutePath());
        media.setInsertDate(LocalDateTime.now());
        media.setDateOriginal(imageAndMetadata.getDateTime());
        media.setMimeType(imageAndMetadata.getMimeType());
        media.setType(imageAndMetadata.getMediaType());
        imageAndMetadata.getGeoLocation().ifPresent(geoLocation -> {
            media.setLatitude(geoLocation.getLatitude());
            media.setLongitude(geoLocation.getLongitude());
        });

        return media;
    }

}
