package fr.gallery.service;

import fr.gallery.dao.CrudDao;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class AbstractCrudService<PK, TYPE, DAO extends CrudDao<PK, TYPE>> {

    final DAO dao;

    protected AbstractCrudService(DAO dao) {
        this.dao = dao;
    }

    /**
     * @see CrudDao#all()
     */
    public Flux<TYPE> all() {
        return Flux.fromStream(dao.all().stream());
    }

    /**
     * @see CrudDao#count()
     */
    public Mono<Integer> count() {
        return Mono.just(dao.count());
    }

    /**
     * @see CrudDao#byId(Object)
     */
    public Mono<TYPE> byId(PK id) {
        return Mono.just(dao.byId(id));
    }

    /**
     * @see CrudDao#create(Object)
     */
    public Mono<Void> create(TYPE object) {
        dao.create(object);
        return Mono.empty();
    }

    /**
     * @see CrudDao#update(Object)
     */
    public Mono<Void> update(TYPE object) {
        dao.update(object);
        return Mono.empty();
    }

    /**
     * @see CrudDao#delete(Object)
     */
    public Mono<Void> delete(PK id) {
        dao.delete(id);
        return Mono.empty();
    }

    /**
     * @see CrudDao#exists(Object)
     */
    public Mono<Boolean> exists(PK id) {
        return Mono.just(dao.exists(id));
    }
}
