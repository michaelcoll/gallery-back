-- utilisateur
INSERT INTO utilisateur (id, username, password, email, actif)
VALUES (42, 'JohnDoe', 'S3cretP@ss', 'mail@mail.com', TRUE);

-- visibilité
INSERT INTO visibility (id, name)
VALUES (1, 'All');

-- media
INSERT INTO media (hash, name, disk_location, description, insert_date, date_original, gps_longitude,
                   gps_latitude, mime_type, owner_id, visibility_id)
VALUES ('ABCD', 'ma super photo', '/un/dossier/sur/le/disque/image.jpg', 'ma super photo', '2019-08-15',
        '2019-08-01', -0.055889, 48.38319, 'image/jpeg', 42, 1),
       ('ABCDE', 'ma super photo2', '/un/dossier/sur/le/disque/image2.jpg', 'ma super photo', '2019-08-15',
        '2019-08-01', -0.055889, 48.38319, 'image/jpeg', 42, 1),
       ('ABCD', 'ma super photo', '/un/dossier/sur/le/disque/image3.jpg', 'ma super photo', '2019-08-15',
        '2019-08-01', -0.055889, 48.38319, 'image/jpeg', 42, 1);
