package fr.gallery.dao;

import fr.gallery.bean.Media;
import fr.gallery.bean.User;
import fr.gallery.bean.Visibility;
import fr.gallery.dao.config.TestDaoConfiguration;
import org.flywaydb.test.annotation.FlywayTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {TestDaoConfiguration.class})
@FlywayTest(locationsForMigrate = {"sql"})
public class MediaDaoTest extends AbstractDaoTest {

    @Autowired
    private MediaDao dao;

    @Test
    public void test_create() {
        Media media = new Media();
        media.setName("new media");
        media.setHash("AZERTY");
        media.setDiskLocation("/");
        media.setMimeType("image/jpeg");
        media.setOwner(new User(42));
        media.setVisibility(new Visibility(1L));

        dao.create(media);

        media = dao.byId(3L);
        assertThat(media)
            .isNotNull()
            .hasFieldOrPropertyWithValue("hash", "AZERTY")
            .hasFieldOrPropertyWithValue("name", "new media")
            .hasFieldOrPropertyWithValue("mimeType", "image/jpeg");
    }

    @Test
    public void test_byId() {
        Media media = dao.byId(0L);

        assertThat(media)
            .isNotNull().hasFieldOrPropertyWithValue("name", "ma super photo");
        assertThat(media.getOwner())
            .isNotNull().hasFieldOrPropertyWithValue("username", "JohnDoe");
        assertThat(media.getVisibility())
            .isNotNull().hasFieldOrPropertyWithValue("name", "All");

    }

    @Test
    public void test_all() {
        List<Media> users = dao.all();

        assertThat(users).hasSize(3);
    }

    @Test
    public void test_update() {
        Media media = dao.byId(0L);

        String expectedName = "un nouveau nom";
        media.setName(expectedName);

        dao.update(media);

        media = dao.byId(0L);
        assertThat(media).isNotNull();
        assertThat(media.getName()).isEqualTo(expectedName);
    }

    @Test
    public void test_delete() {
        dao.delete(1L);

        assertThat(dao.exists(1L)).isFalse();
    }

    @Test
    public void test_byHash() {
        List<Media> medias = dao.byHash("ABCD");

        assertThat(medias).hasSize(2);
    }
}
