package fr.gallery.dao.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(TestConfiguration.class)
public class TestDaoConfiguration {

    private static final String DRIVER_CLASSNAME = "org.hsqldb.jdbcDriver";
    private static final String JDBC_URL = "jdbc:hsqldb:mem:test";

    @Bean(destroyMethod = "close")
    public HikariDataSource testDataSource() {
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(5);
        config.setDriverClassName(TestDaoConfiguration.DRIVER_CLASSNAME);
        config.setJdbcUrl(TestDaoConfiguration.JDBC_URL);
        config.setUsername(null);
        config.setPassword(null);
        return new HikariDataSource(config);
    }

    @Bean
    public Flyway flyway(@Qualifier("testDataSource") HikariDataSource dataSource) {

        return Flyway.configure()
            .dataSource(dataSource)
            .load();
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(@Qualifier("testDataSource") HikariDataSource dataSource) {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        return factoryBean;
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer configurer = new MapperScannerConfigurer();
        configurer.setBasePackage("fr.gallery.dao");
        configurer.setSqlSessionFactoryBeanName("sqlSessionFactoryBean");
        return configurer;
    }
}
